from flask import Flask
from wand.image import Image
from wand.exceptions import PolicyError


def resize_image(url_image, id):
    with Image(filename=url_image) as img:
        with img.clone() as i:
            new_heigth = 500
            i.resize(int(i.width / i.height * new_heigth), new_heigth)
            i.save(filename='result-{}.png'.format(str(id)))


app = Flask(__name__)


@app.route('/')
def hello():

    str_errors = ''

    try:
        url_sample_local = 'dummy.pdf'
        resize_image(url_sample_local, 2)
    except PolicyError as e:
        str_errors += '<br>Wand PolicyError: ' + str(e)

    return 'Hello World!<br>' + str_errors


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
