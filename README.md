This is a minimal reproducible code for https://issuetracker.google.com/issues/182294537.

Install dependencies

```
pip install -r requirements.txt
```

Run the server:

```
python main.py
```

Deploy to App Engine

```
gcloud config set project <PROJECT_ID>
gcloud app deploy
```
